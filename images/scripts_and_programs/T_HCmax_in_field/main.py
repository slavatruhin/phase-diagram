import numpy as np
import numba
import matplotlib.pyplot as plt
import os
import re

# JN - Fero
# J-N - AntiFero
# J0 - SG
@numba.njit()
def calc(e, g, T, N, M, H):
    stat = g * np.exp(-e/T)
    z = stat.sum()
    p = stat/z
    e1 = (e * p).sum()
    e2 = (e**2 * p).sum()
    return (e2 - e1**2) / (N * T**2)


@numba.njit(parallel=True)
def parallel_calc(e, g, T, N, M, H):
    res = np.zeros(T.size)
    for i in numba.prange(T.size):
        res[i] = calc(e, g, T[i], N, M, H)
    return(res)


dirr = "data/dos81/"
plt.figure(dpi=(300))
#plt.yscale("log")
#plt.rcParams['font.size'] = '18'
plt.xlabel("$H$, $J$", fontsize=22)
plt.ylabel(r"$T_{max}\cdot k_B$ / $J$", fontsize=22)
#plt.ylabel(r"$C_{max}, k_{B}$", fontsize=22)

labe = []
mark = ['8', 's', 'p', 'P', '*', 'h', 'D', '+', 'x', 'o']
mark = mark[0]
# JN - Fero
# J-N - AntiFero
# J0 - SG


h = np.linspace(0, 6, 40)
#h = np.array([0])
for H in h:
    P = np.array([])
    c_max = np.array([])
    t_max = np.array([])
    for file in os.listdir(dirr):
        if "gem" in file and "J-100_8" in file:
            file = dirr + file
            a = np.loadtxt(file, skiprows=4, dtype="double")
            s = int(open(file).readlines()[0].rstrip())
            j = int(open(file).readlines()[2].rstrip())
            ss = 2*s*(s-1)
            p = (ss + j) / (2*ss)
            N = s*s
            e = a[:, 1]
            g = a[:, 0]
            M = a[:, 2]
            e = e - M*H
            e = (e + abs(np.min(e)))
            r = re.search(r'glass\d+_J[-+]\d+', file)
            T = np.logspace(-2, 2, 100)
            c = parallel_calc(e, g, T, N, M, H)
            #labe.append(r.group(0))
            labe.append(f"N={N}, p={p:.4f}")
            #plt.plot(T, c, lw=3)
            c_max = np.append(c_max, c.max())
            t_max = np.append(t_max, T[np.argmax(c)])
            P = np.append(P, p)
            #print(f"C_max({b}) = {np.max(c)}")
    for m in range(len(mark)):
        plt.scatter([H for i in range(t_max.size)][m], t_max[m], marker=mark[m], s=10, c="black")
labe = set(labe)
plt.legend(labe, fontsize=16)
plt.ylim(bottom=0)
plt.xlim(left=0)
plt.savefig(f"Tmax(H)_N={N}_p={p:.4f}.eps", format='eps', bbox_inches='tight')