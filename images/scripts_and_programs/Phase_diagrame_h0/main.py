import numpy
import matplotlib.pyplot as plt

hc_ms = numpy.loadtxt("data/hc_ms_max_J_h0.010000.dat")
hc_T_max = numpy.load("data/t_max_h0.0.npy")
P_plus_statsum = numpy.load("data/P_+_h0.0.npy")
out = numpy.loadtxt("data/out.txt")
special_dots = numpy.loadtxt("data/special_dots.txt")
P_plus_probability = numpy.loadtxt("data/Phase_dotsP_plus_h0.0.txt")
fm_phase_dots = numpy.loadtxt("data/Phase_dots_T_h0.0.txt")
cell100_compressed = numpy.loadtxt("data/cell100_compressed.csv", delimiter=";", skiprows=1)
cell400_compressed = numpy.loadtxt("data/cell400_compressed.csv", delimiter=";", skiprows=1)
cell900_compressed = numpy.loadtxt("data/cell900_compressed.csv", delimiter=";", skiprows=1)
cell1600_compressed = numpy.loadtxt("data/cell1600_compressed.csv", delimiter=";", skiprows=1)
plt.figure(dpi=300)
# plt.grid(True)
plt.xlim([0, 1])
plt.ylim([0, 3.2])
plt.scatter(out[:, :1], out[:, 2:3], s=18, color='black', marker='*')
plt.scatter(out[:, 1:2], out[:, 2:3], s=18, color='black', marker='*')
# for i in range(len(hc_ms[:1, :])):
#     plt.scatter(hc_ms[:, 4:5], hc_ms[:, 2:3], s=12, color='black', marker='+')
plt.scatter(P_plus_statsum, hc_T_max, s=12, color='black', marker='+')
plt.scatter(P_plus_probability, fm_phase_dots, s=12, color='blue')
# for i in range(len(cell100_compressed[:, 0])):
#     plt.scatter(cell100_compressed[:, 0], cell100_compressed[:, 2], s=12, color='red', marker='+')
# for i in range(len(cell400_compressed[:, 0])):
#     plt.scatter(cell400_compressed[:, 0], cell400_compressed[:, 2], s=12, color='yellow', marker='+')
# for i in range(len(cell900_compressed[:, 0])):
#     plt.scatter(cell900_compressed[:, 0], cell900_compressed[:, 2], s=12, color='green', marker='+')
plt.scatter(cell1600_compressed[:, 0], cell1600_compressed[:, 2], s=25, color='red', marker='o', facecolors='none')
plt.scatter(special_dots[0], special_dots[1], s=125, color='black', marker='o', facecolors='none')
plt.xlabel("$P_{+}$", fontsize=14)
plt.ylabel("$kT / J$", fontsize=14)
plt.tick_params(axis='both', which='major', labelsize=14)
plt.tight_layout()
plt.savefig('../../' + "Phase_diagram_h0_with_vashche_all" + ".eps", format='eps', dpi=1000)
plt.show()
