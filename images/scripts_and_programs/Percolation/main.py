import matplotlib.pyplot as plt
import numpy

percolation = numpy.loadtxt("data/Percolation.dat")
plt.scatter(percolation[:, 0], percolation[:, 1], s=8, color='black')
plt.xlabel("$P_+$", fontsize=18)
plt.ylabel("$P(FM, AFM)$", fontsize=18)
plt.tick_params(axis='both', which='major', labelsize=12)
plt.savefig("data/Percolation.eps", format='eps', dpi=1000)
plt.show()
