import numpy as np
import matplotlib.pyplot as plt
import numba
import os
import configparser

conf = configparser.ConfigParser()
conf.read("data/conf.ini")
dos_dir = "data/dos" + conf["Sample"]["spins"] + "/"
stat_dir = "data/statsum" + conf["Sample"]["spins"] + "/"
external_dir = "data/external" + conf["Sample"]["spins"] + "/"
T = np.linspace(float(conf["Temperature"]["start"]),
                float(conf["Temperature"]["stop"]),
                int(conf["Temperature"]["steps"]))
H = np.linspace(float(conf["Field"]["start"]),
                float(conf["Field"]["stop"]),
                int(conf["Field"]["steps"]))


@numba.njit(parallel=True)
def fm_probability_Egs(g, m, e_total, mass_t, n):
    ferro_probability = np.zeros(mass_t.size)
    for t in numba.prange(mass_t.size):
        ferro_probability[t] = calc_fm_probability_Egs(g, m, e_total, mass_t[t], n)
    return ferro_probability


@numba.njit
def calc_fm_probability_Egs(g, m, e_total, t, n):
    stat = g * np.exp(-e_total / t)
    z = stat.sum()
    prob = stat / z
    energy = e_total * prob
    e_min = min(energy)
    g_gs = 0
    m_gs_fm = 0
    for i in range(len(energy)):
        if energy[i] <= e_min:
            g_gs += g[i]
            if abs(m[i]) >= abs(m_gs_fm):
                m_gs_fm = m[i]
    if g_gs > 0:
        # return (2 / g_gs) * (abs(m_gs_fm)) / (n * n)
        return (abs(m_gs_fm)) / (n * n)
    else:
        return 0


h = float(conf["Plot"]["field"])
print("h = ", h)
P_plus = np.array([])
p_fm = np.zeros(shape=(0, int(conf["Temperature"]["steps"])))
p_afm = np.zeros(shape=(0, int(conf["Temperature"]["steps"])))
J_sum = np.array([])
max_gem_size = 0
arr_of_files = np.array([])
for file in os.listdir(dos_dir):
    file = dos_dir + file
    arr_of_files = np.append(arr_of_files, file)
    N = int(open(file).readlines()[0].rstrip())
    J_sum = int(open(file).readlines()[2].rstrip())
    P_plus = np.append(P_plus, (2 * N * (N - 1) + J_sum) / (4 * N * (N - 1)))
idx = P_plus.argsort()
P_plus = P_plus[idx]
arr_of_files = arr_of_files[idx]
for sample in range(len(os.listdir(dos_dir))):
    file = arr_of_files[sample]
    gem = np.loadtxt(open(file), skiprows=4).T
    G = gem[0]
    E = gem[1]
    M = gem[2]
    E_total = E - M * h
    p_fm = np.append(p_fm, [fm_probability_Egs(G, M, E_total, T, N)], axis=0)
for temp in range(len(T)):
    for p in range(len(P_plus)):
        if p_fm[p, temp] >= 0.85:
            break
    plt.scatter(P_plus[p], T[temp], s=8, color='blue')
    plt.xlim([0, 1])
    plt.xlabel("$P_{+}$", fontsize=18)
    plt.ylabel("$kT/J$", fontsize=18)
    plt.tick_params(axis='both', which='major', labelsize=12)
P_plus_statsum = np.loadtxt("data/external64/P_+_h1.0.csv", delimiter=";")
t_max_h1 = np.loadtxt("data/external64/t_max_h1.0.csv", delimiter=";")
t_max_h4 = np.loadtxt("data/external64/t_max_h4.0.csv", delimiter=";")
cell1600_metropolis_H1 = np.loadtxt("data/external64/cell1600_H1_compressed.csv", delimiter=";", skiprows=1)
cell1600_metropolis_H4 = np.loadtxt("data/external64/cell1600_H4_compressed.csv", delimiter=";", skiprows=1)
if h == 1:
    T_carbone_AI = np.linspace(0.2, 3.0, 8)
    for temp in range(len(T_carbone_AI)):
        carbone_AI = [[[0.135, 0.802],
                       [0.106, 0.817],
                       [0.126, 0.844],
                       [0.111, 0.871],
                       [0.108, 0.882],
                       [0.102, 0.923],
                       [0.066, 0.927],
                       [0.06, 0.945]],
                      [[0.13, 0.73],
                       [0.1, 0.731],
                       [0.082, 0.74],
                       [0.058, 0.77],
                       [0.052, 0.791],
                       [0.047, 0.89],
                       [0.05, 0.903],
                       [0.026, 0.941]]]
        plt.scatter(carbone_AI[1][temp][0], T_carbone_AI[temp], s=18, color='black', marker='*')
        plt.scatter(carbone_AI[1][temp][1], T_carbone_AI[temp], s=18, color='black', marker='*')
    plt.ylim([0, 3.8])
    plt.scatter(P_plus_statsum, t_max_h1, s=12, color='black', marker='+')
    plt.scatter(cell1600_metropolis_H1[:, 0], cell1600_metropolis_H1[:, 2], s=25, color='red', marker='o', facecolors='none')
if h == 4:
    plt.scatter(P_plus_statsum, t_max_h4, s=12, color='black', marker='+')
    plt.scatter(cell1600_metropolis_H4[:, 0], cell1600_metropolis_H4[:, 2], s=25, color='red', marker='o', facecolors='none')
if conf["Plot"]["write"] == "True":
    plt.savefig("../../" + conf["Plot"]["plot"] + "1_h" + str(h) + ".eps", format='eps', dpi=1000)
plt.show()
