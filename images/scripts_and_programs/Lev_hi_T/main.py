import os
import numpy as np
import matplotlib.pyplot as plt


dos_dir = "../dos/dos64/"

# for file in os.listdir(dos_dir):
file = "gem_glass64_J0_8.dat"
filename = dos_dir + file
J = int(open(filename).readlines(10)[2])
N = int(open(filename).readlines(10)[0])
# if J != 0:
#     continue
G, E, M = np.loadtxt(filename, skiprows=4).T
P_plus = (2 * N * (N - 1) + J) / (4 * N * (N - 1))
t = np.logspace(-1, 1, 100)
h = np.arange(0.06, 0.31, 0.03)
gs = np.min(E)
# plt.title(file)
chi_max = np.array([])
t_max = np.array([])
for H in h:
    m_mean = np.array([])
    m2_mean = np.array([])
    for T in t:
        plt.xscale('log')
        E_h = E + M * H
        gs_h = np.min(E_h)
        E_h -= gs_h
        p = G * np.exp(-((E_h) / T))
        Z = p.sum()
        m_mean = np.append(m_mean, np.sum(M * p / Z))
        m2_mean = np.append(m2_mean, np.sum(M * M * p / Z))
    chi = (m2_mean - m_mean ** 2) / t
    chi_max = np.append(chi_max, np.max(chi))
    t_max = np.append(t_max, t[np.argmax(chi)])
    plt.plot(t, chi, label='{:.2}'.format(H))
    plt.legend()

    # t_max = np.append(t_max, t[np.argmax((m2_mean - m_mean**2) / t)])

    # plt.plot(t_max, h)
plt.plot(t_max, chi_max, '-o',  label='Chi')
plt.xlabel("kT/J", fontsize=14)
plt.ylabel("$\chi$", fontsize=14)
plt.tick_params(axis='both', which='major', labelsize=14)
plt.savefig('../../Chi(kT)_J0_8.eps', format='eps', dpi=300)
plt.show()
