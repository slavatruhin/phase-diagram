import numpy as np
import matplotlib.pyplot as plt
import os


dos_dir = "../dos/"

def risovach(p_search):
    all_c = np.array([])
    all_n = np.array([])
    for dirname in os.listdir(dos_dir):
        if 'dos' not in dirname:
            continue
        # print(dirname)
        for file in os.listdir(dos_dir + dirname):
            filename = dos_dir + dirname + '/' + file
            J = int(open(filename).readlines(10)[2])
            N = int(open(filename).readlines(10)[0])
            P_plus = (2 * N * (N - 1) + J) / (4 * N * (N - 1))
            if abs(P_plus - p_search) > 2e-2:
                continue
            # print(filename)
            G, E, M = np.loadtxt(filename, skiprows=4).T

            # t =  np.logspace(-0.5, 0.7, 100)
            t = np.logspace(-1, 1, 100)
            gs = np.min(E)
            E -= gs
            c_max = np.array([])
            e_mean = np.array([])
            e2_mean = np.array([])
            for T in t:
                # plt.xscale('log')
                p = G * np.exp(-(E / T))
                Z = p.sum()
                e_mean = np.append(e_mean, np.sum(E * p / Z))
                e2_mean = np.append(e2_mean, np.sum(E * E * p / Z))
            # plt.plot(t, e_mean + gs_h, label=f'{H}')
            c_max = np.append(c_max, np.max((e2_mean - e_mean ** 2) / (t * N * N)))
            # print(f'{H} -> {t_max}')
        plt.ylim([0.3, 2.6])
        try:

            all_c = np.append(all_c, np.mean(c_max))
            all_n = np.append(all_n, N * N)
        except:
            pass
    # plt.plot(all_n, all_c, label='p={:.2}'.format(p_search))
    plt.scatter(all_n, all_c, s=5)
    f = np.poly1d(np.polyfit(all_n, all_c, 1, full=True)[0])
    plt.plot(np.arange(25, 82, 1), f(np.arange(25, 82, 1)), label='p={:.2}'.format(p_search))
    plt.xlabel("N", fontsize=14)
    plt.ylabel("Cmax", fontsize=14)
    plt.tick_params(axis='both', which='major', labelsize=14)


for i in np.arange(1., 0.4, -0.1):
    risovach(i)
    plt.legend()
plt.savefig('../../Cmax(N)_scaling.eps', format='eps', dpi=300)
plt.show()
