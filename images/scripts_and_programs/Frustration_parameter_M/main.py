import os
import numba
import numpy
import matplotlib.pyplot as plt
import configparser
import time

start = time.time()

conf = configparser.ConfigParser()
conf.read("data/conf.ini")
dos_dir = "data/dos" + conf["Sample"]["spins"] + "/"
stat_dir = "data/statsum" + conf["Sample"]["spins"] + "/"

T = numpy.linspace(float(conf["Temperature"]["start"]),
                   float(conf["Temperature"]["stop"]),
                   int(conf["Temperature"]["steps"]))
H = numpy.linspace(float(conf["Field"]["start"]),
                   float(conf["Field"]["stop"]),
                   int(conf["Field"]["steps"]))


@numba.njit(parallel=True)
def frustration_parameter(g, e_total, e, m, field, mass_t, n):
    frus_param = numpy.zeros(mass_t.size)
    for i in numba.prange(mass_t.size):
        frus_param[i] = calc_fp(g, e_total, e, m, field, mass_t[i], n)
    return frus_param


@numba.njit
def calc_fp(g, e_total, e, m, field, t, n):
    stat = g * numpy.exp(-e_total / t)
    z = stat.sum()
    p = stat / z
    m_aver = (m * field * p).sum()
    # e_min = numpy.min(e_total)
    # e_max = 2 * (n - 1) * n
    m_max = numpy.max(e_total)
    return (m_max + m_aver) / (2 * m_max)


file_count = 0
for file in os.listdir(dos_dir):
    file_count += 1
if conf["Recalculate"]["recalculate"] == "True":
    for h in range(len(H)):
        P_plus = numpy.array([])
        fp = numpy.zeros(shape=(0, int(conf["Temperature"]["steps"])))
        for file in os.listdir(dos_dir):
            file = dos_dir + file
            gem = numpy.loadtxt(open(file), skiprows=4).T
            N = int(open(file).readlines()[0].rstrip())
            J_sum = int(open(file).readlines()[2].rstrip())
            P_plus = numpy.append(P_plus, (2 * N * (N - 1) + J_sum) / (4 * N * (N - 1)))
            G = gem[0]
            E = gem[1]
            M = gem[2]
            E_total = E - M * H[h]
            # E_total += abs(numpy.min(E_total))
            fp = numpy.append(fp, [frustration_parameter(G, E_total, E, M, H[h], T, N)], axis=0)
        numpy.save(stat_dir + "P_+_h" + str(h), P_plus)
        numpy.save(stat_dir + "fp_h" + str(h), fp)
label = []
# plt.figure(dpi=300)
h = 0
print("h = ", H[h])
for temp in range(len(T)):
    P_plus_loaded = numpy.array([])
    fp_loaded = numpy.zeros(shape=(file_count, int(conf["Temperature"]["steps"])))
    for file in os.listdir(dos_dir):
        P_plus_loaded = numpy.load(stat_dir + "P_+_h" + str(H[h]) + ".npy")
        fp_loaded = numpy.load(stat_dir + "fp_h" + str(H[h]) + ".npy")
    plt.scatter(P_plus_loaded, fp_loaded[:, temp], s=5)
    plt.xlabel("$P_{+}$")
    plt.ylabel("$Fp$")
    label.append(f"T = {T[temp]}")
    plt.legend(label)
end = time.time() - start
print(end)
plt.show()
