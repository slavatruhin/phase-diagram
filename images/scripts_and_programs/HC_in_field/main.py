import os
import numba
import matplotlib.pyplot as plt
import numpy as np


@numba.njit
def calc_c(g, e, t):
    stat = g * np.exp(-e / t)
    z = stat.sum()
    p = stat / z
    e1 = (e * p).sum()
    e2 = (e ** 2 * p).sum()
    return (e2 - e1 ** 2) / (36 * t ** 2)


@numba.njit(parallel=True)
def a_lot_of_calc(g, e, t):
    c = np.zeros(t.size)
    for i in numba.prange(t.size):
        c[i] = calc_c(g, e, t[i])
    return c


label = []
h = np.linspace(0, 5, 5)
for H in h:
    c_max = np.array([])
    t_max = np.array([])
    p_plus = np.array([])
    for file in os.listdir("data/dos/"):
        file = "data/dos/" + file
        a = np.loadtxt(file, skiprows=4).T
        n = int(open(file).readlines()[0].rstrip())
        J = int(open(file).readlines()[2].rstrip())
        p_plus = np.append(p_plus, (2 * n * (n - 1) + J) / (4 * n * (n - 1)))
        G = a[0]
        E = a[1]
        M = a[2]
        E = E - M * H
        E = E + abs(np.min(E))
        T = np.logspace(-2, 2, 100)
        hc = a_lot_of_calc(G, E, T)
        c_max = np.append(c_max, np.max(hc))
        t_max = np.append(t_max, np.argmax(hc))
    plt.scatter(p_plus, c_max, s=0.8, c="black")
    label.append(f"H = {H}")
    plt.legend(label)
    plt.xlabel("$P_{+}$")
    plt.ylabel("$C_{max}$")
plt.show()
